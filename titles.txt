20436464 : Transcript assembly and quantification by RNA-Seq reveals unannotated transcripts and isoform switching during cell differentiation
19453446 : A whole-genome snapshot of 454 sequences exposes the composition of the barley genome and provides evidence for parallel evolution of genome size in wheat and barley
16093701 : SINEs and LINEs: symbionts of eukaryotic genomes with a common tail
12226222 : Nucleotides and nucleotide sugars in developing maize endosperms: synthesis of ADP-glucose in
17474978 : Comparative sequence analysis of
17241472 : Creating a honey bee consensus gene set
20436464 : Transcript assembly and quantification by RNA-Seq reveals unannotated transcripts and isoform switching during cell differentiation
15059830 : Fragment assembly with short reads
20019144 : assembly of human genomes with massively parallel short read sequencing
0 : Sequencing the potato genome: outline and first results to come from the elucidation of the sequence of the world's third most important crop
16093699 : Repbase Update, a database of eukaryotic repetitive elements
17440619 : Assessing performance of orthology detection strategies applied to eukaryotic genomes
0 : Nuclear DNA content of some important plant species
17947255 : i-ADHoRe 2.0: an improved tool to detect degenerated genomic homology using genomic profiles
7584439 : The value of prior knowledge in discovering motifs with MEME
19838736 : Genome-wide identification of NBS-encoding resistance genes in
12016059 : An HMM model for coiled-coil domains and a comparison with PSSM-based predictions
16317077 : Paircoil2: improved predictions of coiled coils from sequence
19906694 : PRGdb: a bioinformatics platform for plant resistance gene analysis
19207213 : cv. Heinz 1706 chromosome 6: distribution and abundance of genes and retrotransposable elements
0 : Patterns of embryological and biochemical evolution in the Asterids
18832442 : Unraveling ancient hexaploidy through multiply-aligned angiosperm gene maps
21289626 : Hotspots of aberrant epigenomic reprogramming in human induced pluripotent stem cells
19269371 : Parkinson’s disease patient-derived induced pluripotent stem cells free of viral reprogramming factors
20682450 : Chromatin structure and gene expression programs of human embryonic and induced pluripotent stem cells
19330000 : Targeted bisulfite sequencing reveals changes in DNA methylation associated with nuclear reprogramming
19881528 : Differential methylation of tissue- and cancer-specific CpG island shores distinguishes human induced pluripotent stem cells, embryonic stem cells and fibroblasts
20644535 : Epigenetic memory in induced pluripotent stem cells
20644536 : Cell type of origin influences the molecular and functional properties of mouse induced pluripotent stem cells
20418860 : Aberrant silencing of imprinted genes on chromosome 12qF1 in mouse induced pluripotent stem cells
19590502 : Variation in the safety of induced pluripotent stem cell lines
20160098 : Neural differentiation of human induced pluripotent stem cells follows developmental principles but with variable potency
19829295 : Human DNA methylomes at base resolution show widespread epigenomic differences
0 : Human and mouse adipose-derived cells support feeder-independent induction of pluripotent stem cells
19345179 : A fresh look at iPS cells
19265657 : Broader implications of defining standards for the pluripotency of iPSCs
12426580 : BMP4 initiates human embryonic stem cell differentiation to trophoblast
19308066 : Linking DNA methylation and histone modification: patterns and paradigms
20512117 : Relationship between nucleosome positioning and DNA methylation
20133333 : Dynamic changes in the human methylome during differentiation
20452322 : Distinct epigenomic landscapes of pluripotent and lineage-committed human cells
20509949 : Induction of neuro-protective/regenerative genes in stem cells infiltrating post-ischemic brain tissue
16516881 : Molecular mapping of developing dorsal horn-enriched genes by microarray and dorsal/ventral subtractive screening
19570518 : Induced pluripotent stem cells and embryonic stem cells are distinguished by gene expression signatures
16904174 : Induction of pluripotent stem cells from mouse embryonic and adult fibroblast cultures by defined factors
19261174 : Ultrafast and memory-efficient alignment of short DNA sequences to the human genome
19289445 : TopHat: discovering splice junctions with RNA-Seq
16862139 : Feeder-independent culture of human embryonic stem cells
16388305 : Derivation of human embryonic stem cells in defined conditions
18007627 : An adapter ligation-mediated PCR method for high-throughput mapping of T-DNA inserts in the
20416082 : Seeker: precise mapping for bisulfite sequencing
18423832 : Highly integrated single-base resolution maps of the epigenome in
18035408 : Induction of pluripotent stem cells from adult human fibroblasts by defined factors
18029452 : Induced pluripotent stem cell lines derived from human somatic cells
18157115 : Reprogramming of human somatic cells to pluripotency with defined factors
19325077 : Human induced pluripotent stem cells free of vector and transgene sequences
19672241 : iPS cells produce viable mice through tetraploid complementation
19672243 : Adult mice generated from induced pluripotent stem cells
NaN : The oyster genome reveals stress adaptation and complexity of shell formation
21743474 : Genome sequence and analysis of the tuber crop potato
20798317 : Genomic comparison of the ants
20838655 : Multi-platform next-generation sequencing of the domestic turkey (
21832995 : The genome sequence of Atlantic cod reveals a unique immune system
19451168 : Fast and accurate short read alignment with BurrowsâWheeler transform
21170042 : Haplotype-resolved genome sequencing of a Gujarati Indian individual
21813624 : A comprehensively molecular haplotype-resolved genome of a European individual
17616269 : Single nucleotide polymorphisms and their relationship to codon usage bias in the Pacific oyster
9136021 : Fixation, segregation and linkage of allozyme loci in inbred families of the Pacific oyster
20029184 : Molluscan memory of injury: evolutionary insights into chronic pain and neurological disorders
18204455 : Whole-genome sequencing and variant discovery in
20019144 : assembly of human genomes with massively parallel short read sequencing
12612834 : , a novel family of putative transposable elements in bivalve mollusks
20880995 : MITE-Hunter: a program for discovering miniature inverted-repeat transposable elements from genomic sequences
14604796 : Hox and paraHox genes in bivalve molluscs
12220984 : ParaHox genes: evolution of Hox/ParaHox cluster integrity, developmental mode, and temporal colinearity
17097629 : The chemical defensome: environmental sensing and response genes in the
20214925 : The C1q domain containing proteins: where do they come from and what do they do?
21063081 : The primary role of fibrinogen-related proteins in invertebrates is defense, not coagulation
15247481 : Diversification of Ig superfamily genes in an invertebrate
20855590 : Effects of past, present, and future ocean carbon dioxide concentrations on the growth and survival of larval shellfish
21587205 : Cellular stress response pathways and ageing: intricate molecular relationships
21444803 : Discovery and molecular characterization of a Bcl-2-regulated cell death pathway in schistosomes
22416118 : Mitochondrial pathway of apoptosis is ancestral in metazoans
9271409 : Role of the human heat shock protein hsp70 in protection against stress-induced apoptosis
0 : Macromolecules in mollusc shells and their functions in biomineralization
19665573 : Molluscan shell evolution with review of shell calcification hypothesis
15073378 : Hemocyte-mediated shell mineralization in the eastern oyster
17402782 : Macroscopic fibers self-assembled from recombinant miniature spider silk proteins
19167317 : Cell traction forces direct fibronectin matrix assembly
14508477 : Anthropogenic carbon and ocean pH
17067686 : Exosomes: from biogenesis and secretion to biological function
21989406 : ExoCarta 2012: database of exosomal proteins, RNA and lipids
18266856 : The penicillin-binding proteins: structure and role in peptidoglycan biosynthesis
17150393 : Tyrosinase localization in mollusc shells
19582213 : An updated review of tyrosinase inhibitors
17950376 : Molluscan shell proteins: primary structure, origin, and evolution
0 : The case for sequencing the Pacific oyster genome
17095691 : The genome of the sea urchin
17372217 : Extreme genomic variation in a natural population
20010809 : The sequence and
NaN : A physical, genetic and functional sequence assembly of the barley genome
19212403 : The nature of selection during plant domestication
0 : Plant genome size estimation by flow cytometry: inter-laboratory comparison
18832645 : A physical map of the 1-gigabase bread wheat chromosome 3B
17151072 : An improved method to identify BAC clones using pooled overgos
22389690 : Development of high-density genetic maps for barley and wheat using a novel two-enzyme genotyping-by-sequencing approach
21467582 : Unlocking the barley genome by chromosomal and comparative genomics
19965430 : The B73 maize genome: complexity, diversity, and dynamics
19189423 : The Sorghum bicolor genome and the diversification of grasses
18032588 : TEnest: automated chronological annotation and visualization of nested plant transposable elements
8745082 : LTR-retrotransposons and MITEs: important players in the evolution of plant genomes
21415278 : Comprehensive sequence analysis of 24,783 barley full-length cDNAs derived from 12 clone libraries
15888675 : MetaCyc and AraCyc. Metabolic pathway databases for plant research
21622801 : Frequent gene movement and pseudogene evolution is common to the large and complex genomes of wheat, barley, and their relatives
9545207 : The NB-ARC domain: a novel signalling motif shared by plant resistance gene products and regulators of cell death in animals
12172030 : Genome dynamics and evolution of the
15078326 : A single-amino acid substitution in the sixth leucine-rich repeat of barley
22334646 : Evolution of wild cereals during 28 years of global warming in Israel
20192836 : Diversity at the
14555698 : An
19122662 : A transcriptome atlas of rice cell types uncovers cellular, functional and developmental hierarchies
15901503 : Nonsense-mediated mRNA decay: molecular insights and mechanistic variations across species
22127866 : Alternative splicing and nonsense-mediated decay modulate expression of important regulatory genes in
12502788 : Evidence for the widespread coupling of alternative splicing and nonsense-mediated mRNA decay in humans
20795950 : NMD: RNA biology meets human genetic medicine
22384098 : A role for nonsense-mediated mRNA decay in plants: pathogen responses are induced in
22379136 : Aberrant growth and lethality of
22025558 : Nonsense-mediated mRNA decay factors,
20627892 : Function annotation of the rice transcriptome at single-nucleotide resolution by RNA-seq
22337053 : Modular regulatory principles of large non-coding RNAs
0 : Diverse functions of nuclear non-coding RNAs in eukaryotic gene expression
15083273 : Estimating the outcrossing rate of barley landraces and wild barley populations collected from ecologically different regions of Jordan
21076812 : Patterns of polymorphism and linkage disequilibrium in cultivated barley
19961604 : Development and implementation of high-throughput SNP genotyping in barley
19698139 : Evidence and evolutionary analysis of ancient whole-genome duplication in barley predating the divergence from rice
18294901 : To check or not to check? The application of meiotic studies to plant breeding
12586880 : Powdery mildew-induced
0 : A bacterial artificial chromosome library for barley (
0 : Variability in fine structures of noncellulosic cell wall polysaccharides from cereal grains: potential importance in human health and nutrition
11751217 : DNA sequence quality trimming and vector removal
19930547 : 454 sequencing of barcoded BAC pools for comprehensive gene survey and genome analysis in the complex genome of barley
21999860 : Sequencing of BAC pools by different next generation sequencing platforms and strategies
18349386 : Velvet: algorithms for de novo short read assembly using de Bruijn graphs
0 : A new DNA extraction method for high-throughput marker analysis in a large-genome species such as
19368652 : Strong correlation of the population structure of wild barley (
19451168 : Fast and accurate short read alignment with BurrowsâWheeler transform
21573248 : A robust, simple genotyping-by-sequencing (GBS) approach for high diversity species
19455180 : A high-density transcript linkage map of barley derived from a single population
17219208 : A 1000 loci transcript map of the barley genome â new anchoring points for integrative grass genomics
21486237 : Highly parallel gene-to-BAC addressing using microarrays
17944808 : Gene expression quantitative trait locus analysis of 16,000 barley genes reveals a complex pattern of genome-wide transcriptional regulation
0 : Engineering a software tool for gene structure prediction in higher organisms
22383036 : Differential gene and transcript expression analysis of RNA-seq experiments with TopHat and Cufflinks
20436464 : Transcript assembly and quantification by RNA-Seq reveals unannotated transcripts and isoform switching during cell differentiation
12906862 : High-throughput fingerprinting of bacterial artificial chromosomes using the snapshot labeling kit and sizing of restriction fragments by capillary electrophoresis
11076862 : Contigs built with fingerprints, markers, and FPC V4.7
21595870 : BAC library resources for map-based cloning and physical map construction in barley (
NaN : An integrated map of genetic variation from 1,092 human genomes
22604720 : Evolution and functional impact of rare coding variation from deep sequencing of human exomes
18949038 : Genome-wide analysis of single nucleotide polymorphisms uncovers population structure in Northern Europe
21962505 : Clan genomics and the complex architecture of human disease
22291602 : Inference of population structure using dense haplotype data
7901202 : Expression cloning of a novel GalÎ²(1â3/1â4)GlcNAc Î±2,3-sialyltransferase using lectin resistance selection
0 : Sequence variations in the public human genome data reflect a bottlenecked population history
22582263 : Recent explosive human population growth has resulted in an excess of rare genetic variants
0 : Genome-wide patterns of population structure and admixture in West Africans and African Americans
21152010 : Identifying a high fraction of the human genome to be under selective constraint using GERP++
22080510 : KEGG for integration and interpretation of large-scale molecular data sets
17382889 : Analysis of the vertebrate insulator protein CTCF-binding sites in the human genome
0 : Exome sequencing as a tool for Mendelian disease gene discovery
19348700 : The Human Gene Mutation Database: 2008 update
20952405 : COSMIC: mining complete cancer genomes in the Catalogue of Somatic Mutations in Cancer
22384356 : Genotype imputation with thousands of genomes
21829380 : Fine mapping of five loci associated with low-density lipoprotein cholesterol detects variants that double the explained heritability
22801493 : TNF receptor 1 genetic risk mirrors outcome of anti-TNF therapy in multiple sclerosis
20430937 : Fine mapping of the association with obesity at the
19892942 : Human genome sequencing using unchained base reads on self-assembling DNA nanoarrays
0 : Potential etiologic and functional implications of genome-wide association loci for human diseases and traits
0 : Resequencing of positional candidates identifies low frequency
0 : A rare penetrant mutation in
0 : A genome-wide association study identifies new psoriasis susceptibility loci and an interaction between
0 : The mystery of missing heritability: Genetic interactions create phantom heritability
0 : Gene-environment-wide association studies: emerging approaches
22307276 : DNase I sensitivity QTLs are a major determinant of human expression variation
22807667 : Efficiency and power as a function of sequence coverage, SNP array density, and imputation
21293372 : Mapping copy number variation by population-scale genome sequencing
0 : Discovery and genotyping of genome structural polymorphism by sequencing on a population scale
21460063 : Low-coverage sequencing: implications for design of complex trait association studies
0 : assembly and genotyping of variants using colored de Bruijn graphs
20529929 : Efficient construction of an assembly string graph using the FM-index
21030649 : Diversity of human copy number variation and multicopy genes
0 : Genetic loci influencing kidney function and chronic kidney disease
19812545 : Origins and functional impact of copy number variation in the human genome
21917140 : The functional spectrum of low-frequency coding variation
22604722 : An abundance of rare functional variants in 202 drug target genes sequenced in 14,002 people
0 : Differential confounding of rare and common variants in spatially structured populations
0 : Demographic history and rare allele sharing among human populations
NaN : Genome sequence and analysis of the tuber crop potato
0 : Global distribution of the potato crop
17721507 : The grapevine genome sequence suggests ancestral hexaploidization in major angiosperm phyla
19325131 : Plants with double genomes might have had a better chance to survive the Cretaceous–Tertiary extinction event
20972441 : Genome-wide patterns of genetic variation among elite maize inbred lines
10802651 : Gene ontology: tool for the unification of biology
19965431 : A first-generation haplotype map of maize
2200713 : Gene expression during tuber development in potato plants
0 : Members of the Kunitz-type protease inhibitor gene family of potato inhibit soluble tuber invertase
10929100 : Antisense inhibition of plastidial phosphoglucomutase provides compelling evidence that potato tuber amyloplasts import carbon from the cytosol in the form of glucose-6-phosphate
20028468 : Glucose 1-phosphate is efficiently taken up by potato (
0 : Nutrients, bioactive non-nutrients and anti-nutrients in potatoes
11495763 : Control of potato tuber sprouting
16183837 : activates
18247136 : Genome-wide identi?cation of
16167894 : The R1 resistance gene cluster contains three groups of independently evolving, type I R1 homologues and shows substantial structural variation among haplotypes of Solanum demissum.
15494555 : Multiple genetic processes result in heterogeneous rates of evolution within the major cluster disease resistance genes in lettuce
19741609 : Genome sequence and analysis of the Irish potato famine pathogen
16582432 : Construction of a 10,000-marker ultradense genetic recombination map of potato: providing a framework for accelerated gene isolation and a genomewide physical map
0 : Using RepeatMasker to identify repetitive elements in genomic sequences
0 : Influence of culture medium and in vitro conditions on shoot regeneration in Solanum phureja monoploids and fertility of regenerated doubled monoploids
19289445 : TopHat: discovering splice junctions with RNA-Seq
12952885 : OrthoMCL: identification of ortholog groups for eukaryotic genomes
19497933 : SOAP2: an improved ultrafast tool for short read alignment
9254694 : Gapped BLAST and PSI-BLAST: a new generation of protein database search programs
10827456 : EMBOSS: the European Molecular Biology Open Software Suite
11591649 : SSAHA: a fast search method for large DNA databases
18502942 : : a tool for annotating primate segmental duplications
19037014 : Identification of miniature inverted-repeat transposable elements (MITEs) and biogenesis of their siRNAs in the Solanaceae: new functional implications for MITEs
15215400 : AUGUSTUS: a web server for gene finding in eukaryotes
9149143 : Prediction of complete gene structures in human genomic DNA
15123596 : GeneWise and Genomewise
19263082 : Genome-wide analysis of
14681412 : The KEGG resource for deciphering the genome
19774472 : Assignment of genetic linkage maps to diploid
