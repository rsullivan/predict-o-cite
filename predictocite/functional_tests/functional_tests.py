from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)

	def tearDown(self):
		self.browser.quit()


	def test_can_add_article_title_and_references_and_retrieve_it_later(self):
		# editor has received a submission and wants to 
		# analyse the title and the papers references
		# to get an idea of how many citations it is likely
		# to attract. They go to the predict-o-cite service
		self.browser.get('http://localhost:8000')

		# editor notices the page title and header mention 
		# the name of the service

		self.assertIn('Predict-O-Cite', self.browser.title)

		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('Predict-O-Cite', header_text)

		# editor is invited to enter the title of the research paper
		input_box = self.browser.find_element_by_id('article_data')
		self.assertEqual(
			input_box.get_attribute('placeholder'), 'Enter your research paper\'s title'
			)


		# editor types [SOME TITLE] into a text box
		input_box.send_keys('Hotspots of aberrant epigenomic reprogramming in human induced pluripotent stem cells')

		# when the editor hits enter, the page updates with the title

		input_box.send_keys(Keys.ENTER)

		table = self.browser.find_element_by_id('article_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertTrue(
			any(row.text == 'Title: Hotspots of aberrant epigenomic reprogramming in human induced pluripotent stem cells' for row in rows),
			"Article title does not appear in table")


		self.fail('Finish the test!')

		

		

		

		# the editor wants to add the paper's references so clicks
		# on the "Add References" button.

		# a text box appears and the editor adds the doi of the
		# first reference 

		# the page updates with the doi

		# to add another reference the editor clicks on Add Reference button
		# and adds another doi

		# the editor has finished entering the dois and clicks on the Submit button.

		# the data entered is returned as a unique URL.

		# satisfied with the results the editor "goes to sleep".

if __name__ == '__main__':
	unittest.main(warnings='ignore')

